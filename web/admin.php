<?php

require_once(dirname(__FILE__).'/../config/ProjectConfiguration.class.php');

if (isset($_SERVER['ENV']) && ($_SERVER['ENV']=='local_jorge' || $_SERVER['ENV']=='local_fer')){
	$ambiente='dev';
	$debug=true;
}else{	
	$ambiente="prod";
	$debug=false;
}

$configuration = ProjectConfiguration::getApplicationConfiguration('admin', $ambiente, $debug);
sfContext::createInstance($configuration)->dispatch();
