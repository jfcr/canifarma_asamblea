<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body>


      
      <div id="wrap">
      
          <div class="container"> 

            <?php echo $sf_content ?>
  
          </div> <!-- /container -->
    
      </div>
      
      
      <div id="footer">
      <div class="container">
        <!--<p class="text-muted credit">Example courtesy <a href="http://martinbean.co.uk">Martin Bean</a> and <a href="http://ryanfait.com/sticky-footer/">Ryan Fait</a>.</p>-->
        <p class="text-muted credit pull-right">Cámara Nacional de la Industria Farmacéutica: Av. Cuauhtemoc #1481, Col. Santa Cruz Atoyac, 03310, México D.F. Tel. 5688 9477
Todos los derechos reservados CANIFARMA
        | Powered by KML media. </p>
      </div>
    </div>
  </body>
</html>
