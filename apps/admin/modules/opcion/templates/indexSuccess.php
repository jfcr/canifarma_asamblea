<h1>Opcions List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Email administrador</th>
      <th>Remitente email</th>
      <th>Remitente nombre</th>
      <th>Asunto</th>
      <th>Registro exitoso email</th>
      <th>Registro exitoso administrador</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($opcions as $opcion): ?>
    <tr>
      <td><a href="<?php echo url_for('opcion/edit?id='.$opcion->getId()) ?>"><?php echo $opcion->getId() ?></a></td>
      <td><?php echo $opcion->getEmailAdministrador() ?></td>
      <td><?php echo $opcion->getRemitenteEmail() ?></td>
      <td><?php echo $opcion->getRemitenteNombre() ?></td>
      <td><?php echo $opcion->getAsunto() ?></td>
      <td><?php echo $opcion->getRegistroExitosoEmail() ?></td>
      <td><?php echo $opcion->getRegistroExitosoAdministrador() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('opcion/new') ?>">New</a>
