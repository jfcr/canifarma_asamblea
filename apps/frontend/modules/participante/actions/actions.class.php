<?php

/**
 * participante actions.
 *
 * @package    canifarma-formulario
 * @subpackage participante
 * @author     Your name here
 */
class participanteActions extends sfActions
{

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new participanteForm();
  }

  public function executeGracias(sfWebRequest $request)
  {
    
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new participanteForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }


  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $participante = $form->save();

      $this->redirect('participante/gracias?id='.$participante->getId());
    }
  }
}
