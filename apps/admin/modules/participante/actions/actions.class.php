<?php

/**
 * participante actions.
 *
 * @package    canifarma-formulario
 * @subpackage participante
 * @author     Your name here
 */
class participanteActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->participantes = ParticipantePeer::doSelect(new Criteria());
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new participanteForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new participanteForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($participante = ParticipantePeer::retrieveByPk($request->getParameter('id')), sprintf('Object participante does not exist (%s).', $request->getParameter('id')));
    $this->form = new participanteForm($participante);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($participante = ParticipantePeer::retrieveByPk($request->getParameter('id')), sprintf('Object participante does not exist (%s).', $request->getParameter('id')));
    $this->form = new participanteForm($participante);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($participante = ParticipantePeer::retrieveByPk($request->getParameter('id')), sprintf('Object participante does not exist (%s).', $request->getParameter('id')));
    $participante->delete();

    $this->redirect('participante/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $participante = $form->save();

      $this->redirect('participante/edit?id='.$participante->getId());
    }
  }
}
