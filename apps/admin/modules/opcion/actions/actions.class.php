<?php

/**
 * opcion actions.
 *
 * @package    canifarma-formulario
 * @subpackage opcion
 * @author     Your name here
 */
class opcionActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->opcions = OpcionPeer::doSelect(new Criteria());
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new opcionForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new opcionForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($opcion = OpcionPeer::retrieveByPk($request->getParameter('id')), sprintf('Object opcion does not exist (%s).', $request->getParameter('id')));
    $this->form = new opcionForm($opcion);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($opcion = OpcionPeer::retrieveByPk($request->getParameter('id')), sprintf('Object opcion does not exist (%s).', $request->getParameter('id')));
    $this->form = new opcionForm($opcion);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($opcion = OpcionPeer::retrieveByPk($request->getParameter('id')), sprintf('Object opcion does not exist (%s).', $request->getParameter('id')));
    $opcion->delete();

    $this->redirect('opcion/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $opcion = $form->save();

      $this->redirect('participante/index');
    }
  }
}
