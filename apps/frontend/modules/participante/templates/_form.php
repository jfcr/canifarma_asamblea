<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<div class="jumbotron">
<form action="<?php echo url_for('participante/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table class="table">
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'participante/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" class="btn btn-info" value="Guardar" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <td colspan="2"> <h4>Datos del Participante</h4> </td>
      </tr>
      <tr>
        <th>Empresa o Entidad:</th>
        <td>
          <?php echo $form['empresa']->renderError() ?>
          <?php echo $form['empresa'] ?>
        </td>
      </tr>
      <tr>
        <th>Título Profesional:</th>
        <td>
          <?php echo $form['titulo_profesional']->renderError() ?>
          <?php echo $form['titulo_profesional'] ?>
        </td>
      </tr>
      <tr>
        <th>Nombre del Participante:</th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['apellido_paterno']->renderLabel() ?>:</th>
        <td>
          <?php echo $form['apellido_paterno']->renderError() ?>
          <?php echo $form['apellido_paterno'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['apellido_materno']->renderLabel() ?>:</th>
        <td>
          <?php echo $form['apellido_materno']->renderError() ?>
          <?php echo $form['apellido_materno'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['puesto']->renderLabel() ?>:</th>
        <td>
          <?php echo $form['puesto']->renderError() ?>
          <?php echo $form['puesto'] ?>
        </td>
      </tr>
      <tr>
        <th>Teléfono con clave lada:</th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>
        </td>
      </tr>
      <tr>
        <th>E-mail:</th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>
        </td>
      </tr>
<tr>
  <td colspan="2"> <h4>Datos del Contacto</h4> </td>
</tr>
      <tr>
        <th>Nombre:</th>
        <td>
          <?php echo $form['contacto_nombre']->renderError() ?>
          <?php echo $form['contacto_nombre'] ?>
        </td>
      </tr>
      <tr>
        <th>Teléfono con clave lada:</th>
        <td>
          <?php echo $form['contacto_telefono']->renderError() ?>
          <?php echo $form['contacto_telefono'] ?>
        </td>
      </tr>
      <tr>
        <th>E-mail:</th>
        <td>
          <?php echo $form['contacto_email']->renderError() ?>
          <?php echo $form['contacto_email'] ?>
        </td>
      </tr>
<!--
      <tr>
        <th><?php echo $form['created_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_at']->renderError() ?>
          <?php echo $form['created_at'] ?>
        </td>
      </tr>
-->      
    </tbody>
  </table>
</form>
</div>