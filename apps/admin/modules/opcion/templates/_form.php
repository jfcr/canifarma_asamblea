<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('opcion/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;
          <?php endif; ?>
          <input type="submit" value="Actualizar" class="btn btn-info" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['email_administrador']->renderLabel() ?></th>
        <td>
          <?php echo $form['email_administrador']->renderError() ?>
          <?php echo $form['email_administrador'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['remitente_email']->renderLabel() ?></th>
        <td>
          <?php echo $form['remitente_email']->renderError() ?>
          <?php echo $form['remitente_email'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['remitente_nombre']->renderLabel() ?></th>
        <td>
          <?php echo $form['remitente_nombre']->renderError() ?>
          <?php echo $form['remitente_nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['asunto']->renderLabel() ?></th>
        <td>
          <?php echo $form['asunto']->renderError() ?>
          <?php echo $form['asunto'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['registro_exitoso_email']->renderLabel() ?></th>
        <td>
          <?php echo $form['registro_exitoso_email']->renderError() ?>
          <?php echo $form['registro_exitoso_email'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['registro_exitoso_administrador']->renderLabel() ?></th>
        <td>
          <?php echo $form['registro_exitoso_administrador']->renderError() ?>
          <?php echo $form['registro_exitoso_administrador'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
