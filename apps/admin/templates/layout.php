<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
    <script>
   

      <?php 
        $url= str_replace("request_password", "", url_for(""));
        $url= str_replace("sf_guard_user", "",$url);
      ?>
      var url="<?php echo $url?>";
      var url_general="<?php echo str_replace("sf_guard_permission", "", url_for(""));?>";

      var val_tmp1="";
      var val_tmp2="";
      var val_tmp3="";
      var val_tmp4="";
      var val_tmp5="";

    </script>
  </head>
  <body>
      
      
      <div id="wrap">
      
          <div class="container"> 

              
              <?php if($sf_user->isAuthenticated()):?>
      <!-- Static navbar -->
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Administrador</a>
        </div>
        
        <div class="navbar-collapse collapse">
          <ul  class="nav navbar-nav">
            <li class="active"><a href="<?php echo url_for('participante/index') ?>">Incio</a></li>
            <li><a href="<?php echo url_for('opcion/edit?id=1') ?>">Opciones</a></li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
              
            <li><a href="<?php echo url_for('@sf_guard_signout'); ?>">Salir <span class="glyphicon glyphicon-circle-arrow-right"></span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
        
      </div>
      
      
      <?php endif; ?>
      
    <?php if ($sf_user->hasFlash('success')):?>
    <div class="alert alert-success"><?php echo html_entity_decode($sf_user->getFlash('success'));?></div>
    <?php endif;?>

    <?php if ($sf_user->hasFlash('error')):?>
    <div class="alert alert-danger"><?php echo $sf_user->getFlash('error');?></div>
    <?php endif;?>

      <?php echo $sf_content ?>

    </div> <!-- /container -->
    
    
    
    
      </div>
      
      
      <div id="footer">
      <div class="container">
        <!--<p class="text-muted credit">Example courtesy <a href="http://martinbean.co.uk">Martin Bean</a> and <a href="http://ryanfait.com/sticky-footer/">Ryan Fait</a>.</p>-->
        <p class="text-muted credit pull-right">Cámara Nacional de la Industria Farmacéutica: Av. Cuauhtemoc #1481, Col. Santa Cruz Atoyac, 03310, México D.F. Tel. 5688 9477
Todos los derechos reservados CANIFARMA
        | Powered by KML media. </p>
      </div>
    </div>
  </body>
</html>