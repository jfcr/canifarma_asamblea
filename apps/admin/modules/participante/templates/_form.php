<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('participante/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table class="table" >
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('participante/index') ?>" class="btn btn-warning">Regresar</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'participante/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?', 'class' => 'btn btn-danger')) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar" class="btn btn-info" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['empresa']->renderLabel() ?></th>
        <td>
          <?php echo $form['empresa']->renderError() ?>
          <?php echo $form['empresa'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['titulo_profesional']->renderLabel() ?></th>
        <td>
          <?php echo $form['titulo_profesional']->renderError() ?>
          <?php echo $form['titulo_profesional'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['nombre']->renderLabel() ?></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['apellido_paterno']->renderLabel() ?></th>
        <td>
          <?php echo $form['apellido_paterno']->renderError() ?>
          <?php echo $form['apellido_paterno'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['apellido_materno']->renderLabel() ?></th>
        <td>
          <?php echo $form['apellido_materno']->renderError() ?>
          <?php echo $form['apellido_materno'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['puesto']->renderLabel() ?></th>
        <td>
          <?php echo $form['puesto']->renderError() ?>
          <?php echo $form['puesto'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['telefono']->renderLabel() ?></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['email']->renderLabel() ?></th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['contacto_nombre']->renderLabel() ?></th>
        <td>
          <?php echo $form['contacto_nombre']->renderError() ?>
          <?php echo $form['contacto_nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['contacto_telefono']->renderLabel() ?></th>
        <td>
          <?php echo $form['contacto_telefono']->renderError() ?>
          <?php echo $form['contacto_telefono'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['contacto_email']->renderLabel() ?></th>
        <td>
          <?php echo $form['contacto_email']->renderError() ?>
          <?php echo $form['contacto_email'] ?>
        </td>
      </tr>
      <!--
      <tr>
        <th><?php echo $form['created_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_at']->renderError() ?>
          <?php echo $form['created_at'] ?>
        </td>
      </tr>
    -->
    </tbody>
  </table>
</form>
