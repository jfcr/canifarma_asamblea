<form class="form-signin" action="<?php echo url_for('@sf_guard_signin') ?>" method="post" id="form-signin">
    <?php echo $form->renderHiddenFields(false) ?>
    <?php echo $form->renderGlobalErrors() ?>
    
    
    
    <h2 class="form-signin-heading">Administrador</h2>
    <div class="sign" id="div-signin">
        <input name="signin[username]" type="text" class="form-control" placeholder="Username" autofocus>
        <?php echo $form['username']->renderError() ?>
        <input name="signin[password]" type="password" class="form-control" placeholder="Contraseña">
        <?php echo $form['password']->renderError() ?>
        <!--<label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>-->        
    </div>
        <button class="btn btn-lg btn-warning btn-block" id="btn-signin" type="submit" >Ingresar <span class="glyphicon glyphicon-share-alt"></span></button>
</form>


        

